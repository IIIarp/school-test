const path = require('path');

const outPath = path.join(__dirname, '/dist');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/index.js',
  },
  output: {
    filename: 'bundle.js',
    sourceMapFilename: 'bundle.js.map',
    path: outPath,
  },
};
